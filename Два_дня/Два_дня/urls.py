﻿"""
Definition of urls for Два_дня.
"""

from django.views.generic.base import RedirectView
from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views

import app.forms
import app.views

# Uncomment the next lines to enable the admin:
#from django.conf.urls import include
#from django.contrib import admin
#admin.autodiscover()

favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)

urlpatterns = [

    #url(r'^data$', include('app.urls')),
    url(r'^data$', app.views.data, name='Data'),
    url(r'^vivod$', app.views.vidod, name='vivod'),
    url(r'^favicon\.ico$', favicon_view),
    url(r'^search$', app.views.search, name='search'),
    url(r'^recover$', app.views.recover, name='recover'),


    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^about$', app.views.about, name='about'),
    

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
]
