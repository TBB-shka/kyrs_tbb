"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".
"""
######

#from stripogram import html2text


########

from random import choice, uniform
from bs4 import BeautifulSoup
import pandas as pd
from django.shortcuts import render

#######


import django
from django.test import TestCase
import unittest
from app.views import text_before_word
from app.views import get_html
from app.views import to_html
from app.views import to_html_no_search


from app.views import get_page_data_lamoda
from app.views import lamoda
from django.http import HttpRequest
from django.http import HttpResponse
import sys, requests, operator


# TODO: Configure your database in settings.py and sync before running tests.

class ViewTest(TestCase):
    """Tests for the application views."""

    if django.VERSION[:2] >= (1, 7):
        # Django 1.7 requires an explicit setup() when running tests in PTVS
        @classmethod
        def setUpClass(cls):
            super(ViewTest, cls).setUpClass()
            django.setup()



    def test_contact(self):
        """Страница контактов"""
        response = self.client.get('/contact')
        self.assertContains(response, 'Contact', 0, 200)

    def test_data(self):
        """Страница поисковика"""
        response = self.client.get('/data')
        self.assertContains(response, 'Data', 0, 200)

    def test_home(self):
        """Начальная страница"""
        response = self.client.get('/')
        self.assertContains(response, 'Home', 0, 200)

    def test_get_html_lamoda(self):
        search=str("шампунь")
        base_url = str('https://www.lamoda.ru/catalogsearch/result/?q=')  
        urlp = str(base_url + search)
        res=get_html(urlp)
        #print(res)
        #K=str('Ошиииибка')
        #Ky=str('Ошиииибка')
        d=isinstance(res, str)
        assert d
        
    def test_text_before_word(self):
        text=str("Super puper group")
        world=str("puper")
        prov=str("Super")
        res=text_before_word(text,world)
        #d=isinstance(res, str)
        #assert d
        self.assertEqual(res,prov)

    def test_test_get_page_data_lamoda(self):
        url='https://www.lamoda.ru/catalogsearch/result/?q=шампунь&page=1'
        r=get_html(url)
        res=[]
        res=get_page_data_lamoda(r)
        d=isinstance(res,list)
        assert d

    def test_lamoda_no_unut(self):
        search=str("шампунь")
        res=lamoda(search)
        d=isinstance(res,list)
        assert d
    
    def test_to_html(self):
        search=str("Шампунь")
        price_ot=int(0)
        price_do=int(99999)
        name=str('Тестовый шампунь')
        price=int(250)
        magazin=str("ТУСУР")
        url=str("Ссылка")
        data=[]
        resu=[]
        data = {'name':name,
                'price':price,
                'magazin':magazin,
                'url':url
                }
        resu.append(data)
        islamoda=bool(True)
        ispudra=bool(True)
        isroskosmetika=bool(False)
        isasos=bool(True)
        panda = pd.DataFrame(resu) 
        to_html(panda[panda.price > price_ot][panda.price < price_do].sort_values('price'), search,islamoda, ispudra, isroskosmetika, isasos)
        #  islamoda, ispudra, isroskosmetika, isasos)
        #res = str(html2text('templates/app/include/include.html'))
        #res=str("Адра кдабра ууууф кабуф Тестовый шампунь габууул")
        #self.assertEqual(name,panda)
        html_file=open("app/templates/app/include/include.html","r", encoding='utf-8')
        res=html_file.read()
        otv=""" 
            {% block content %}


             <div class="col-md-12">
                    <div class="tabl">
                        <h2>Поиск по слову: "
                Шампунь "
                </h2>
                <h2>Количество найденых товаров:1
                </h2>
		            <table border="1" cellpadding="5">
   			            <tr>
   				            <th>Цена</th>
    				            <th>Название</th>
				            <th>Магазин</th>
   			            </tr>
            
                     <tr>
    				            <td>250</td>
    				            <td>Тестовый шампунь</td>
            <th><a href="Ссылка">ТУСУР</a></th>
 			             </tr>            
 		            </table>
                    </div>
                    <div class="magaz">
            
                        </div>
                    </p>

            {%endblock%}
            """
        word=str('Тестовый шампунь')
        if word in res:
            d=bool(True)
        assert d
      
      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        #d=bool(True)
        #self.assertEqual(res,otv)



        
        
        
        
#    def test_prob(sefl):
#        r=int(5)
#        d=int(8)
#        assert d==r

### Не найден атрибут find_all

#    def test_get_page_data_lamoda(self):
#        name=str("Lyi")
#        price=int(10)
#        magazin=str("Letu")
#        urle=str("Qqwweerr")
#        data = {'name':name,
#                'price':price,
#                'magazin':magazin,
#               'url':urle}
#        res=get_page_data_lamoda('https://www.lamoda.ru/catalogsearch/result/?q=шампунь&page=1')
#        d=isinstance(res, data)
#        assert d


