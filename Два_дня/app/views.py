"""
Definition of views.
"""

from django.views.decorators.cache import cache_page



from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponse
from django.template import RequestContext
from datetime import datetime
from app.models import Product

import sys, requests, operator, re
from random import choice, uniform
from bs4 import BeautifulSoup
import pandas as pd

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Начальная страница',
            'year':datetime.now().year,
        }
    )

def recover(request):
    if 'recover' in request.GET:
        recovers=str(request.GET['recover'])
        if recovers=='':
            recovers='Отсутствует код восстановления поиска'
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/form_vivod.html',
        {
            'recover':recovers,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Обратная связь. ',
            #'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'Обратная связь. ',
           # 'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )

def data(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/Data.html',
        {
            'title':'Поисковик',
            #'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def vidod(request):
    print(request)
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
      'app/include/include.html',
        {
        }
    )

@cache_page(600, cache='default', key_prefix='')
def search(request):
    tre = ''
    lamoda='on'
    pudra='on'
    asos='on'
    roskosmetika='on'    
    sort_price = 'yes'
    
    price_ot='0'
    price_do='99999'
    assert isinstance(request, HttpRequest) 
    if 'q' in request.GET:
        tre = str(request.GET['q'])
        tre.replace(' ','+')
    if 'lamoda' in request.GET:
        lamoda=str(request.GET['lamoda'])
    if 'pudra' in request.GET:
        pudra=str(request.GET['pudra'])
    if 'Asos' in request.GET:
        asos=str(request.GET['Asos'])
    if 'roskosmetika' in request.GET:
        roskosmetika=str(request.GET['roskosmetika'])
    if 'price_do' in request.GET:
        price_do=str(request.GET['price_do'])        
    if 'price_ot' in request.GET:
        price_ot=str(request.GET['price_ot'])
    if 'sort_price' in request.GET:
        sort_price=str(request.GET['sort_price'])

    if(lamoda == 'on' or lamoda == 'off'):
        isOklamoda = True
    else:
        isOklamoda = False
    if(pudra == 'on' or pudra == 'off'):
        isOkpudra = True
    else:
        isOkpudra= False
    if(asos == 'on' or asos == 'off'):
        isOkasos = True
    else:
        isOkasos = False
    if(roskosmetika == 'on' or roskosmetika == 'off'):
        isOkroskosmetika = True
    else:
        isOkroskosmetika = False
    if(sort_price == 'yes' or sort_price == 'no'):
        isOksort_price = True
    else:
        isOksort_price = False

    if(isOklamoda == False or isOkpudra == False or isOkasos == False or isOkroskosmetika == False or isOksort_price == False):
        return HttpResponse('Параметры запроса заполнены не правильно')

    patternDigit="[0-9]{0,15}"
    if(price_ot == ''):
        price_ot = 0
    else:
        if(bool(re.match(patternDigit,price_ot))):
           price_ot = int(price_ot)
           if(price_ot < 0):
               price_ot = abs(price_ot)
        else:
            return HttpResponse('Границы цены необходимо указывать цифрами')

    if(price_do == ''):
        price_do = 99999
    else:
        if(bool(re.match(patternDigit,price_do))):
           price_do = int(price_do)
           if(price_do < 0):
               price_do = abs(price_do)
        else:
            return HttpResponse('Границы цены необходимо указывать цифрами')

    if(price_ot > price_do):
        price_ot = 0
        price_do = 99999
    
    
    patternTre = "^[A-Za-zА-Яа-яЁё\s]+$"
    
    if(bool(re.match(patternTre,tre))):
        parcing(tre, lamoda, pudra, roskosmetika, asos, price_ot, price_do, sort_price)
    else:
        return HttpResponse('Поиск должен состоять из букв русского и/или английского алфавита')
    
    return render(
        request,
        'app/vivod.html',
        {
            'title':"Результат вашего поиска, надеюсь удачно",
            'year':datetime.now().year,
            }
        )   

def text_before_word(text, word):
    line = text.split(word)[0].strip()
    return line

def get_html(url, useragent=None, proxy=None):    
    r = requests.get(url, headers=useragent, proxies=proxy)    
    return r.text

def get_page_data_lamoda(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='products-catalog__list').find_all('div', class_='products-list-item')  
    datas = []
    for ad in ads:   
        try:
            name = text_before_word(ad.find('div',class_='products-list-item__brand').text, 'span').replace('/','').replace(',','').replace('"','')
            name += text_before_word(ad.find('span',class_='products-list-item__type').text, 'span').replace('/','').replace(',','').replace('"','')            
        except:
            continue        
        try:
            div = ad.find('a', class_='products-list-item__link').get('href')
            url = "https://www.lamoda.ru" + div
        except:
            continue
        try:
            price = float(ad.find('div', class_='to-favorites').get('data-price'))
        except:
            continue
        magazin = 'lamoda.ru'
            
        data = {'name':name,
                'price':price,
                'magazin':magazin,
                'url':url}
        datas.append(data)

    return datas

def get_page_data_pudra(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='b-pagination_paginated-content').find_all('a', class_='b-grid-product_card-link')  
    datas = []
    for ad in ads:   
        try:        
            name = ad.get('data-category') + '  ' + ad.get('data-brand') + '  ' + ad.get('data-name')            
        except:
            continue       
        try:
            div = ad.get('href')
            url = "https://pudra.ru" + div
        except:
            continue
        try:
            price = float(ad.get('data-price'))
        except:
            continue
        magazin = 'pudra.ru'
            
        data = {'name':name,
                'price':price,
                'magazin':magazin,
                'url':url}
        datas.append(data)

    return datas

def get_page_data_roskosmetika(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='product-list').find_all('div', class_='product-in-list')  
    datas = []    
    for ad in ads:         
        try:        
            name = ad.find('a', class_='product-item__name').text + ' ' + ad.find('div', class_='pack-name').text
            name = name.replace(',','').replace('/','')            
        except:
            continue        
        try:
            div =  ad.find('a', class_='product-item__name').get('href')
            url = "https://www.roskosmetika.ru" + div            
        except:
            continue
        try:
            price = float(text_before_word(ad.find('div', class_='col-xs-4 price').find('div', class_='').text, "р").replace(' ',''))
            if(price == None):
                try:
                    price = float(ad.find('div', class_='col-xs-4 price').find('div', class_='new_price').text.replace(' ','').replace('"',''))
                except:
                    continue            
        except:
            continue
        magazin = 'roskosmetika.ru'
            
        data = {'name':name,
                'price':price,
                'magazin':magazin,
                'url':url}
        datas.append(data)

    return datas

def get_page_data_asos(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='_3-pEc3l').find_all('article', class_='_2oHs74P')  
    datas = []    
    for ad in ads:         
        try:        
            name = ad.find('a', class_='_3x-5VWa').get('aria-label').replace(',','').replace('/','')
        except:
            continue        
        try:            
            url = ad.find('a', class_='_3x-5VWa').get('href')          
        except:
            continue
        try:
            price = float(text_before_word(ad.find('span', class_='_342BXW_').text, " р").replace('"','').replace(',','.'))            
            
        except:
            continue
        magazin = 'asos.com'
            
        data = {'name':name,
                'price':price,
                'magazin':magazin,
                'url':url}
        datas.append(data)

    return datas
    
def lamoda(search):
    base_url = 'https://www.lamoda.ru/catalogsearch/result/?q='
    
    page_part = '&page='    
    urlp = base_url + search
    url = base_url + search + page_part 
 
    #  'https://www.lamoda.ru/catalogsearch/result/?q=шампунь&page=1'
    #useragents = open('useragents.txt').read().split('\n')
    #proxies = open('proxies.txt').read().split('\n')
    #proxy = {'http': 'http://' + choice(proxies)}
    #useragent = {'User-Agent': choice(useragents)}

    soup = BeautifulSoup(get_html(urlp), 'lxml')    
    
    data = []

    if soup.find('h1', class_='search-page__title') != None or soup.find('div', class_='title').text == '404 — Страница не найдена':       
        return data
    else:        
        total_pages = int(soup.find('div', class_='paginator').get('data-pages'))        
        for i in range(1, total_pages+1):           
            data+=(get_page_data_lamoda(get_html(url+str(i))))
        return data  
    
def asos(search):
    base_url = 'https://www.asos.com/ru/search/?q='    
    page_part = '&page='    
    urlp = base_url + search
    url = base_url + search + page_part     

    useragents = open('useragents.txt').read().split('\n')
    #proxies = open('proxies.txt').read().split('\n')
    #proxy = {'http': 'http://' + choice(proxies)}
    useragent = {'User-Agent': choice(useragents)}

    htmlp = get_html(urlp, useragent)
    soup = BeautifulSoup(htmlp, 'lxml')    
    
    data = []
    
    if soup.find('p', class_='WPMLS0N') == None: 
        total_item = int(soup.find('progress', class_='_2hirsSG').get('max'))
        item = int(soup.find('progress', class_='_2hirsSG').get('value'))
        if(total_item == item):            
            data += (get_page_data_asos(htmlp))
        else:
            if(int(total_item/item) < float(total_item/item)):
                total_pages = int(total_item/item)+1
            else:
                total_pages = int(total_item/item)            
            for i in range(1, total_pages+1):           
                data+=(get_page_data_asos(get_html(url+str(i), useragent)))
    return data
    
def pudra(search):
    base_url = 'https://pudra.ru/search/?q='
    per_page = '&items_per_page=1500'    
    url = base_url + search + per_page 
    html = get_html(url)

    #useragents = open('useragents.txt').read().split('\n')
    #proxies = open('proxies.txt').read().split('\n')
    #proxy = {'http': 'http://' + choice(proxies)}
    #useragent = {'User-Agent': choice(useragents)}
    data = []
    soup = BeautifulSoup(html, 'lxml')
    if soup.find('span', class_='b-search-no_result-request') != None:       
        return data    
    else:          
        data = get_page_data_pudra(html)
        return data   

def roskosmetika(search):
    base_url = 'https://www.roskosmetika.ru/search?search='
    page_part = '&p='    
    show_all = '&showAll'
    url = base_url + search + show_all    

    #useragents = open('useragents.txt').read().split('\n')
    #proxies = open('proxies.txt').read().split('\n')
    #proxy = {'http': 'http://' + choice(proxies)}
    #useragent = {'User-Agent': choice(useragents)}

    html = get_html(url)     
    soup = BeautifulSoup(html, 'lxml')
    
    data = [] 
    
    if soup.find('span', id='foundCount') != None:
        data+=(get_page_data_roskosmetika(get_html(url)))
    return data

def to_html(panda, search, islamoda, ispudra, isroskosmetika, isasos):
    html = """
    {% block content %}


 <div class="col-md-12">
        <div class="tabl">
            <h2>Поиск по слову: "
    """
    html += search
    html += """ "
    </h2>
    <h2>Количество найденых товаров:"""
    html += str(len(panda.index))
    html += """
    </h2>
		<table border="1" cellpadding="5">
   			<tr>
   				<th>Цена</th>
    				<th>Название</th>
				<th>Магазин</th>
   			</tr>
            """
    
    for i in panda.index:   
        html += """
         <tr>
    				<td>"""
        html += str(panda.loc[i]['price'])
        html += """</td>
    				<td>"""
        html += panda.loc[i]['name']
        html += '</td>\n<th><a href="'                    
        html += panda.loc[i]['url']
        html += '">'  
        html += panda.loc[i]['magazin']
        html += """</a></th>
 			 </tr>"""
       

    html +="""            
 		</table>
        </div>
        <div class="magaz">
        <h1>Статус поиска</h1>
        <h2>Lamoda:
        """
    if(islamoda == True):
        html +=' товар найден</h2>\n'
    else:
        html +=' товар не найден</h2>\n'
    if(ispudra == True):
        html +='<h2>Pudra: товар найден</h2>\n'
    else:
        html +='<h2>Pudra: товар не найден</h2>\n'
    if(isroskosmetika == True):
        html +='<h2>roskosmetika: товар найден</h2>\n'
    else:
        html +='<h2>roskosmetika: товар не найден</h2>\n'
    if(isasos == True):
        html +='<h2>asos: товар найден</h2>\n'
    else:
        html +='<h2>asos: товар не найден</h2>\n'
    

    html +=""""            
            </div>
       </p>

{%endblock%}
    """
    
    Html_file = open("app/templates/app/include/include.html","w", encoding='utf-8')
    for line in html:
        Html_file.write(line)
    Html_file.close()

def to_html_no_search(search):
    html = """
    {% block content %}


 <div class="col-md-12">
        <div class="tabl">
            <h2>Поиск по слову: "
    """
    html += search
    html += """ "
    </h2>
    <h2>Ничего не найдено, попробуйте изменить поиск</h2>    
        <div class="magaz">  
        
            </div>
    </p>
{%endblock%}
    """
    
    Html_file = open("app/templates/app/include/include.html","w", encoding='utf-8')
    for line in html:
        Html_file.write(line)
    Html_file.close()

def parcing(search, lamodap, pudrap, roskosmetikap, asosp, price_ot, price_do, sort_price):    
    data = []
    datap = []
    dataa = []
    datar = []
    datal = []
    islamoda = False
    ispudra = False
    isroskosmetika = False
    isasos = False
    if(lamodap == 'on'):
        datal = lamoda(search)
        if(datal != []):
            islamoda = True
    if(pudrap == 'on'):
        datap = pudra(search)
        if(datap != []):
            ispudra = True
    if(roskosmetikap == 'on'):
        datar = roskosmetika(search) 
        if(datar != []):
            isroskosmetika = True
    if(asosp == 'on'):
        dataa = asos(search)  
        if(dataa != []):
            isasos = True              
    if(isasos == False and islamoda == False and isroskosmetika == False and ispudra == False):
        to_html_no_search(search)

    data = datal + datap + datar + dataa
    
    if(data != []):        
        panda = pd.DataFrame(data) 
        if(sort_price == 'yes'):
            to_html(panda[panda.price > price_ot][panda.price < price_do].sort_values('price'), search, islamoda, ispudra, isroskosmetika, isasos)
        else:
            to_html(panda[panda.price > price_ot][panda.price < price_do].sort_values('price', ascending=False), search, islamoda, ispudra, isroskosmetika, isasos)
    else:
        to_html_no_search(search)
        

